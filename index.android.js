import React, { Component } from 'react'
import { AppRegistry } from 'react-native'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import createLogger from 'redux-logger'

import reducers from './code/reducers'

const logger = createLogger()
const store = createStore(reducers, applyMiddleware(logger))

import App from './code/views/App'

export default class PronoApp extends Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}

AppRegistry.registerComponent('PronoApp', () => PronoApp)
