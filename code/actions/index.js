export function setInit(initialized) {
    return { type: 'SET_INIT', initialized }
}

export function setView(view) {
    return { type: 'SET_VIEW', view }
}

export function setRaces(races) {
    return { type: 'SET_RACES', races }
}

export function setTeams(teams) {
    return { type: 'SET_TEAMS', teams }
}

export function setRiders(riders) {
    return { type: 'SET_RIDERS', riders }
}

export function setGroups(groups) {
    return { type: 'SET_GROUPS', groups }
}

export function setGroup(group) {
    return { type: 'SET_GROUP', group }
}
