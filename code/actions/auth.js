export function setAuth(auth) {
    return { type: 'SET_AUTH', auth }
}

export function setUser(user) {
    return { type: 'SET_USER', user }
}