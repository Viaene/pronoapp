export function setRace(race) {
    return { type: 'SET_RACE', race }
}

export function setChoiceIndex(index) {
    return { type: 'SET_CHOICEINDEX', index }
}
