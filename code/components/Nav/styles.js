import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    nav: {
        backgroundColor: '#ff1742',
        height: 50,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },

    linkTouch: {
        height: 50,
        justifyContent: 'center',
        paddingLeft: 15,
        paddingRight: 15
    },
    
    linkText: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#fff'
    }

})
