import React, { Component, PropTypes } from 'react'
import { TouchableHighlight, Text } from 'react-native'

import styles from './styles'

class NavLink extends Component {
    render() {
        const { label = '', onPress } = this.props

        return (
            <TouchableHighlight onPress={onPress} style={styles.linkTouch} activeOpacity={1} underlayColor="rgba(0,0,0,.1)">
                <Text style={styles.linkText}>{label}</Text>
            </TouchableHighlight>
        )
    }
}

NavLink.propTypes = {
    label: PropTypes.string,
    onPress: PropTypes.func.isRequired
}

export default NavLink
