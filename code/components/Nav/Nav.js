import React, { Component, PropTypes } from 'react'
import { View } from 'react-native'

import NavLink from './Link'
import styles from './styles'

class Nav extends Component {
    render() {
        const { items } = this.props

        const LinkNodes = items.map((item, i) => {
            return (<NavLink key={i} label={item.label} onPress={item.onPress} />)
        })

        return (
            <View style={styles.nav}>
                { LinkNodes }
            </View>
        )
    }
}

Nav.propTypes = {
    items: PropTypes.array.isRequired
}

export default Nav
