import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    list: {
        paddingLeft: 20,
        paddingRight: 20
    },

    item: {
        height: 48,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#fafafa'
    },
    itemText: {
        fontWeight: 'bold',
        fontSize: 13,
        color: '#111111'
    }
})

export default styles
