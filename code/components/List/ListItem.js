import React, { Component, PropTypes } from 'react'
import { TouchableHighlight, Text } from 'react-native'

import styles from './styles'

class ListItem extends Component {
    render() {
        const { label, onPress = () => {} } = this.props

        return (
            <TouchableHighlight onPress={onPress} style={styles.item} activeOpacity={1} underlayColor="rgba(0,0,0,.1)">
                <Text style={styles.itemText}>{label.toUpperCase()}</Text>
            </TouchableHighlight>
        )
    }
}

ListItem.propTypes = {
    onPress: PropTypes.func,
    label: PropTypes.string.isRequired
}

export default ListItem
