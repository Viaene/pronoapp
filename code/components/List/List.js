import React, { Component, PropTypes } from 'react'
import { View } from 'react-native'

import ListItem from './ListItem'
import styles from './styles'

class List extends Component {
    render() {
        const { items } = this.props

        const itemNodes = items.map((item, i) => (<ListItem key={i} {...item} />))

        return (
            <View style={styles.list}>
                {itemNodes}
            </View>
        )
    }
}

List.propTypes = {
    items: PropTypes.array.isRequired
}

export default List
