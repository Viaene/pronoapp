import React, { Component, PropTypes } from 'react'
import { View, TextInput, ScrollView } from 'react-native'

import List from './../List'

import styles from './styles'

class Search extends Component {
    constructor(props) {
        super(props)

        this.state = {
            searchTxt: ''
        }
    }

    render() {
        const { items } = this.props
        const { searchTxt } = this.state

        let result

        //Filter items
        if(searchTxt != '') {
            const regex = new RegExp(searchTxt, "gi")

            result = items.filter((item) => {
                return item.label.match(regex)
            })
        } else {
            result = items
        }

        return (
            <View style={styles.search}>

                <TextInput style={styles.input} placeholder="Search..." value={searchTxt} onChangeText={(txt) => this.setState({searchTxt: txt})} />

                <ScrollView style={styles.groupContent}>
                     <List items={result} />
                </ScrollView>

            </View>
        )
    }
}

Search.propTypes = {
    items: PropTypes.array.isRequired
}

export default Search
