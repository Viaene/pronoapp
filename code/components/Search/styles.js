import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({

    search: {

    },

    input: {
        paddingLeft: 20,
        height: 50,
        backgroundColor: '#eee',
        color: '#111111'
    }

})

export default styles
