import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    header: {
        paddingTop: 30,
        paddingBottom: 15,
        position: 'relative',
        backgroundColor: '#fafafa'
    },
    headerText: {
        textAlign: 'center'
    },


    link: {
        height: 30,
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },

    linkBack: {
        position: 'absolute',
        left: 10,
        top: 23,
        zIndex: 2
    }

})
