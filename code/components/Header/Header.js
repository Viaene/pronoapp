import React, { Component, PropTypes } from 'react'
import { View, Text, Touc } from 'react-native'

import styles from './styles'
import Link from './Link'

class Header extends Component {
    render() {
        const { onBack, title } = this.props

        return (
            <View style={styles.header}>
                { onBack && <Link label="Back" onPress={onBack} /> }

                <Text style={styles.headerText}>{ title }</Text>

            </View>
        )
    }
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    onBack: PropTypes.func
}

export default Header
