import React, { Component, PropTypes } from 'react'
import { TouchableHighlight, Text } from 'react-native'

import styles from './styles'

class HeaderLink extends Component {
    render() {
        const { label = '', onPress } = this.props

        return (
            <TouchableHighlight onPress={onPress} style={[styles.link, styles.linkBack]} activeOpacity={1} underlayColor="rgba(0,0,0,.1)">
                <Text style={styles.linkText}>{label}</Text>
            </TouchableHighlight>
        )
    }
}

HeaderLink.propTypes = {
    label: PropTypes.string,
    onPress: PropTypes.func.isRequired
}

export default HeaderLink
