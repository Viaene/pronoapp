const initialState = {
    initialized: false,
    view: 'Races',
    races: [],
    teams: [],
    riders: [],
    group: '',
    groups: {}
}

const app = (state = initialState, action) => {

    switch (action.type) {
    case 'SET_INIT':
        return {
            ...state,
            initialized : action.initialized
        }
    case 'SET_VIEW':
        return {
            ...state,
            view : action.view
        }

    //Data

    case 'SET_RACES':
        return {
            ...state,
            races : action.races
        }
    case 'SET_TEAMS':
        return {
            ...state,
            teams : action.teams
        }
    case 'SET_RIDERS':
        return {
            ...state,
            riders : action.riders
        }
    case 'SET_GROUPS':
        return {
            ...state,
            groups : action.groups
        }
    case 'SET_GROUP':
        return {
            ...state,
            group : action.group
        }
    default:
        return state
    }

}


export default app
