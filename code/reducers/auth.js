const initialState = {
    group: 'hombres',
    auth: false,
    user: {}
}

const auth = (state = initialState, action) => {

    switch (action.type) {
    case 'SET_USER':
        return {
            ...state,
            user : action.user
        }
    case 'SET_AUTH':
        return {
            ...state,
            auth : action.auth
        }
    case 'SET_GROUP':
        return {
            ...state,
            group : action.group
        }
    case 'SET_CHOICES':
        return {
            ...state,
            user: {
                ...state.user,
                choices: action.choices
            }
        }

    default:
        return state
    }

}


export default auth
