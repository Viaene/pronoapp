import { combineReducers } from 'redux'

import app from './app'
import race from './race'
import auth from './auth'

export default combineReducers({ app, race, auth })
