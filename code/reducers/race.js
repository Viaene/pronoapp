const initialState = {
    race: {},
    choiceIndex: 0
}

const race = (state = initialState, action) => {

    switch (action.type) {
    case 'SET_RACE':
        return {
            ...state,
            race : action.race
        }
    case 'SET_CHOICEINDEX':
        return {
            ...state,
            choiceIndex: action.index
        }
    default:
        return state
    }

}


export default race
