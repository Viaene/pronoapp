import React, { Component, PropTypes } from 'react'
import { Text, View } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Group from './../Group'
import styles from './styles'

class Ranking extends Component {
    render() {
        const { group } = this.props

        const rankItems = group.ranking.map((rank, i) => {
            const user = group.users.find((user) => user._id == rank.userId)
            return (
                <View style={styles.record} key={i}>
                    <Text style={[styles.recordTxt, styles.recordNr]}>{i + 1}</Text>
                    <Text style={[styles.recordTxt, styles.recordLbl]}>{user.name}</Text>
                    <Text style={[styles.recordTxt, styles.recordPoints]}>{rank.points}</Text>
                </View>
            )
        })

        return (
            <Group headerTitle="Ranking">
                <View style={styles.ranking}>
                    {rankItems}
                </View>
            </Group>
        )
    }
}

Ranking.propTypes = {
    group: PropTypes.object.isRequired
}

export default connect(
    (state) => ({
        group: state.app.groups[state.app.group]
    }),
    (dispatch) => ({actions: bindActionCreators(Object.assign({}), dispatch)})
)(Ranking)
