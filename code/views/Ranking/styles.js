import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    ranking: {
        paddingLeft: 20,
        paddingRight: 20
    },
    record: {
        height: 48,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#fafafa',
        flexDirection: 'row',
        alignItems: 'center'
    },
    recordTxt: {
        fontWeight: 'bold',
        fontSize: 13,
        color: '#111111'
    },
    recordNr: {
        width: 20
    },
    recordLbl: {
        flex: 1
    },
    recordPoints: {
        width: 30
    }
})

export default styles
