import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    app: {
        flex: 1
    },
    loading: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default styles
