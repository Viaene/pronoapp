import React, { Component, PropTypes } from 'react'
import { View, Text } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as AppActions from './../../actions'
import styles from './styles'

import Ranking from './../Ranking'
import Races from './../Races'
import Race from './../Race'
import ChooseRider from './../ChooseRider'
import Auth from './../Auth'

class App extends Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    renderApp() {
        const { view } = this.props

        return (
            <View style={styles.app}>
                { view == 'Races' && <Races />}
                { view == 'Ranking' && <Ranking />}
                { view == 'Race' && <Race />}
                { view == 'ChooseRider' && <ChooseRider />}
            </View>
        )
    }

    renderLoading() {
        return (
            <View style={[styles.app, styles.loading]}>
                <Text>Loading...</Text>
            </View>
        )
    }

    renderAuth() {
        return (
            <View style={styles.app}>
                <Auth />
            </View>
        )
    }

    render() {
        const { initialized, auth } = this.props

        if(auth) {
            return initialized ? this.renderApp() : this.renderLoading()
        } else {
            return this.renderAuth()
        }
    }

}

App.propTypes = {
    auth: PropTypes.bool.isRequired,
    user: PropTypes.object.isRequired,
    view: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired
}

export default connect(
    (state) => ({
        auth: state.auth.auth,
        user: state.auth.user,
        initialized: state.app.initialized,
        view: state.app.view
    }),
    (dispatch) => ({ actions: bindActionCreators(Object.assign(AppActions), dispatch) })
)(App)
