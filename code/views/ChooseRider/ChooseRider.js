import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { postJSON } from './../../lib/helpers'
import * as AuthActions from './../../actions/auth'
import * as AppActions from './../../actions'
import routes from './../../config/routes'

import Group from './../Group'
import Search from './../../components/Search'

class ChooseRider extends Component {
    render() {
        const { riders, choiceIndex, race, group, groups, user, actions } = this.props

        const items = riders.map((rider) => {
            return {
                label: rider.name,
                onPress: () => {
                    const _groups = {...groups}
                    _groups[group].races[race._id][user._id][choiceIndex] = rider._id

                    actions.setGroups(_groups)
                    actions.setView('Race')

                    postJSON(`${routes.api}choices/`, {
                        userId: user._id,
                        groupId: group,
                        raceId: race._id,
                        riders: _groups[group].races[race._id][user._id]
                    })
                }
            }
        })

        return (
            <Group headerTitle="Kies een renner" headerOnBack={ () => {actions.setView('Race') }}>
                <Search items={items} />
            </Group>
        )
    }
}

ChooseRider.propTypes = {
    riders: PropTypes.array.isRequired,
    choiceIndex: PropTypes.number.isRequired,
    race: PropTypes.object.isRequired,
    group: PropTypes.string.isRequired,
    groups: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
}

export default connect(
    (state) => ({
        riders: state.app.riders,
        choiceIndex: state.race.choiceIndex,
        race: state.race.race,
        user: state.auth.user,
        group: state.app.group,
        groups: state.app.groups
    }),
    (dispatch) => ({ actions: bindActionCreators(Object.assign(AuthActions, AppActions), dispatch) })
)(ChooseRider)
