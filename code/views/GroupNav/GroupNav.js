import React, { PropTypes, Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Nav from './../../components/Nav'
import * as AppActions from './../../actions'

class GroupNav extends Component {
    render() {
        const { actions } = this.props

        return (
            <Nav items={[
                { label: 'Races', onPress: () => { actions.setView('Races') } },
                { label: 'Ranking', onPress: () => { actions.setView('Ranking') } }
            ]} />
        )
    }
}

GroupNav.propTypes = {
    actions: PropTypes.object.isRequired
}

export default connect(
    () => ({}),
    (dispatch) => ({ actions: bindActionCreators(Object.assign(AppActions), dispatch) })
)(GroupNav)
