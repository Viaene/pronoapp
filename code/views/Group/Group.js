import React, { Component, PropTypes } from 'react'
import { View, ScrollView } from 'react-native'

import Header from './../../components/Header'
import GroupNav from './../GroupNav'
import styles from './styles'

class Group extends Component {
    render() {
        const { headerOnBack, headerTitle } = this.props

        return (
            <View style={styles.group}>
                <Header onBack={headerOnBack} title={headerTitle}/>
                <ScrollView style={styles.groupContent}>
                     { this.props.children }
                </ScrollView>
                <GroupNav />
            </View>
        )
    }
}

Group.propTypes = {
    children: PropTypes.node,
    headerOnBack: PropTypes.func,
    headerTitle: PropTypes.string
}

export default Group
