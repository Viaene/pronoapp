import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    group: {
        flex: 1
    },
    groupContent: {
        flex: 1
    }

})
