import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({

    auth: {
        flex: 1,
        justifyContent: 'center'
    },
    
    authBtnHolder: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'center'
    },

    btn: {
        height: 50,
        backgroundColor: '#3b5998',
        paddingLeft: 30,
        paddingRight: 30,
        borderRadius: 4
    },
    btnTouch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnLbl: {
        color: '#fff',
        fontSize: 13,
        fontWeight: 'bold'
    }

})

export default styles
