import React, { Component, PropTypes } from 'react'
import { View, Text, TouchableHighlight } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { postJSON } from './../../lib/helpers'
import routes from './../../config/routes'
import * as AuthActions from './../../actions/auth'
import * as AppActions from './../../actions/'
import styles from './styles'

import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk'

class Auth extends Component {
    constructor(props) {
        super(props)

        this.login = this.login.bind(this)
        this.getUserInfo = this.getUserInfo.bind(this)
        this.devAuth = this.devAuth.bind(this)
    }

    getUserInfo() {
        const { actions } = this.props

        const infoRequest = new GraphRequest('/me', null, (error, result) => {
            if(error) {
                console.log(error)
            } else {
                AccessToken.getCurrentAccessToken().then((data) => {
                    const user = {
                        name: result.name,
                        fbId: result.id,
                        fbAccessToken: data.accessToken.toString()
                    }

                    postJSON(`${routes.api}data/`, {user}).then((data) => {
                        console.log(data)
                        actions.setRaces(data.races)
                        actions.setTeams(data.teams)
                        actions.setRiders(data.riders)
                        actions.setGroups(data.groups)
                        actions.setGroup('58b34ca0a471f7c4ba40df95')
                        //actions.setGroup('58c16053825b171eced65d8a')
                        actions.setUser(data.user)
                        actions.setInit(true)
                    })

                    actions.setAuth(true)
                })
            }
        })

        new GraphRequestManager().addRequest(infoRequest).start()
    }

    devAuth() {
        const { actions } = this.props

        postJSON(`${routes.api}data/`, {user: {fbId: '0'}}).then((data) => {
            console.log(data)
            actions.setRaces(data.races)
            actions.setTeams(data.teams)
            actions.setRiders(data.riders)
            actions.setGroups(data.groups)
            actions.setGroup('58b34ca0a471f7c4ba40df95')
            //actions.setGroup('58c16053825b171eced65d8a')
            actions.setUser(data.user)
            actions.setInit(true)
        })

        actions.setAuth(true)
    }

    componentDidMount() {
        //this.devAuth()
    }

    login() {
        LoginManager.logInWithReadPermissions(['public_profile']).then((result) => {
            if (result.isCancelled) {
                alert('Login cancelled')
            } else {
                this.getUserInfo()
            }
        },
        (error) => {
            alert('Login fail with error: ' + error)
        })
    }

    render() {
        return (
            <View style={styles.auth}>
                <View style={styles.authBtnHolder}>
                    <View style={styles.btn}>
                        <TouchableHighlight onPress={this.login} style={styles.btnTouch} activeOpacity={1} underlayColor="rgba(0,0,0,.1)">
                            <Text style={styles.btnLbl}>Login with Facebook</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        )
    }
}

Auth.propTypes = {
    actions: PropTypes.object.isRequired
}

export default connect(
    () => ({}),
    (dispatch) => ({actions: bindActionCreators(Object.assign(AppActions, AuthActions), dispatch)})
)(Auth)
