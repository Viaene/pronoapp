import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as AppActions from './../../actions/'
import * as RaceActions from './../../actions/race'
import Group from './../Group'
import List from './../../components/List'

class Race extends Component {

    constructor(props) {
        super(props)

        this.findRider = this.findRider.bind(this)
        this.initChoices = this.initChoices.bind(this)
    }

    findRider(_id) {
        const { riders } = this.props

        return riders.find((rider) => { return rider._id == _id })
    }

    initChoices() {
        const { user, race, groups, group, actions } = this.props
        const choices = []

        for(let i = 0; i < 10; i++) {
            choices[i] = false
        }

        let updateGroups = {...groups}
        updateGroups[group._id].races[race._id][user._id] = choices

        actions.setGroups(updateGroups)

        return choices
    }

    render() {
        const { user, race, group, actions } = this.props

        const groupChoices = group.races[race._id]
        let yourChoices

        if(!groupChoices.hasOwnProperty(user._id)) {
            yourChoices = this.initChoices()
        } else {
            yourChoices = groupChoices[user._id]
        }

        const yourChoicesNodes = yourChoices.map((choice, i) => {
            const choiceItem = { label: `${i + 1}. ${choice ? this.findRider(choice).name : 'Kies een renner'}` }

            return {
                label: `${i + 1}. ${choice ? this.findRider(choice).name : 'Kies een renner'}`,
                onPress: () => {
                    actions.setChoiceIndex(i)
                    actions.setView('ChooseRider')
                }
            }
        })

        return (
            <Group headerOnBack={ () => { actions.setView('Races') }} headerTitle={race.name}>
                <List items={yourChoicesNodes} />
            </Group>
        )
    }

}

Race.propTypes = {
    user: PropTypes.object.isRequired,
    race: PropTypes.object.isRequired,
    groups: PropTypes.object.isRequired,
    group: PropTypes.object.isRequired,
    riders: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
}

export default connect(
    (state) => ({
        user: state.auth.user,
        race: state.race.race,
        riders: state.app.riders,
        groups: state.app.groups,
        group: state.app.groups[state.app.group]
    }),
    (dispatch) => ({ actions: bindActionCreators(Object.assign(AppActions, RaceActions), dispatch) })
)(Race)
