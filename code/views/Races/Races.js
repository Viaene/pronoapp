import React, { Component, PropTypes } from 'react'
import { View } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as AppActions from './../../actions'
import * as RaceActions from './../../actions/race'

import Group from './../Group'
import List from './../../components/List'

class Races extends Component {
    render() {
        const { races, actions } = this.props
        const raceItems = races.map((race) => {
            return {
                label: race.name,
                onPress: () => {
                    actions.setRace(race)
                    actions.setView('Race')
                }
            }
        })

        return (
            <Group headerTitle="Wedstrijden">
                <List items={raceItems} />
            </Group>
        )
    }
}

Races.propTypes = {
    races: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
}

export default connect(
    (state) => ({
        races: state.app.races
    }),
    (dispatch) => ({ actions: bindActionCreators(Object.assign(AppActions, RaceActions), dispatch) })
)(Races)
