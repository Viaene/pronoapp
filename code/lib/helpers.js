export const postJSON = function(url, data){
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then((response) => {
        return response.json()
    })
}

export const getJSON = function(url){
    return fetch(url, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then((response) => {
        return response.json()
    })
}
